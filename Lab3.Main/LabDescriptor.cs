﻿using System;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IMycie);
        public static Type I2 = typeof(IKontrola);
        public static Type I3 = typeof(Izasilanie);

        public static Type Component = typeof(Myjnia);

        public static GetInstance GetInstanceOfI1 = (Component)=>Component;
        public static GetInstance GetInstanceOfI2 = (Component) => Component;
        public static GetInstance GetInstanceOfI3 = (Component) => Component;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(Zasilacz);
        public static Type MixinFor = typeof(Izasilanie);

        #endregion
    }
}
