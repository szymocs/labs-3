﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    class Delegujaca
    {
        private delegate object Delegat(Myjnia myjnia);
        private Delegat delegat;

        public object Delegacja(Myjnia myjnia)
        {
            if (delegat != null)
            {
                return delegat(myjnia);
            }
            return null;
        }
    }
}
